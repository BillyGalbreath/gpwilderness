package net.pl3x.bukkit.gpwilderness;

import net.pl3x.bukkit.gpwilderness.command.CmdGPWilderness;
import net.pl3x.bukkit.gpwilderness.configuration.Config;
import net.pl3x.bukkit.gpwilderness.configuration.Lang;
import net.pl3x.bukkit.gpwilderness.listener.BukkitListener;
import org.bukkit.plugin.java.JavaPlugin;

public class GPWilderness extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            Logger.error("# GriefPrevention NOT found and/or enabled!");
            Logger.error("# This plugin requires GriefPrevention to be installed and enabled!");
            return;
        }

        getServer().getPluginManager().registerEvents(new BukkitListener(), this);

        getCommand("gpwilderness").setExecutor(new CmdGPWilderness(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    public static GPWilderness getPlugin() {
        return GPWilderness.getPlugin(GPWilderness.class);
    }
}

package net.pl3x.bukkit.gpwilderness.configuration;

import net.pl3x.bukkit.gpwilderness.GPWilderness;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static boolean CANCEL_BELOW_SEA_LEVEL = true;
    public static boolean CANCEL_EVENTS = false;
    public static int NOTIFICATION_COOLDOWN = 60;
    public static List<String> PROTECT_WORLDS;
    public static List<String> WILD_WORLDS;

    public static void reload() {
        GPWilderness plugin = GPWilderness.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        CANCEL_BELOW_SEA_LEVEL = config.getBoolean("cancel-below-sea-level", true);
        CANCEL_EVENTS = config.getBoolean("cancel-events", false);
        NOTIFICATION_COOLDOWN = config.getInt("notification-cooldown", 60);
        PROTECT_WORLDS = config.getStringList("protect-worlds");
        WILD_WORLDS = config.getStringList("wild-worlds");
    }
}

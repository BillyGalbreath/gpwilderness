package net.pl3x.bukkit.gpwilderness.configuration;

import net.pl3x.bukkit.gpwilderness.GPWilderness;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;
    public static String WILDERNESS_PROTECTED;
    public static String WILD_WORLD;
    public static String VERSION;
    public static String RELOAD;

    public static void reload() {
        GPWilderness plugin = GPWilderness.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");
        WILDERNESS_PROTECTED = config.getString("wilderness-protected", "&4The wilderness is protected");
        WILD_WORLD = config.getString("wild-world", "&dWelcome to the Wilderness!\n&dThe only rules here are no hacking/xraying. Everything else is fair game.\n&4Be warned, this world gets reset every week.");
        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}

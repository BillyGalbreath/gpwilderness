package net.pl3x.bukkit.gpwilderness.hook;

import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.Location;

public class GPHook {
    public static boolean isClaimed(Location location) {
        return GriefPrevention.getPlugin(GriefPrevention.class).dataStore.getClaimAt(location, false, null) != null;
    }
}

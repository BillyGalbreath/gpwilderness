package net.pl3x.bukkit.gpwilderness.listener;

import net.pl3x.bukkit.gpwilderness.GPWilderness;
import net.pl3x.bukkit.gpwilderness.configuration.Config;
import net.pl3x.bukkit.gpwilderness.configuration.Lang;
import net.pl3x.bukkit.gpwilderness.hook.GPHook;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class BukkitListener implements Listener {
    private Set<UUID> notificationCooldown = new HashSet<>();

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        checkEvent(event, event.getBlock().getLocation(), event.getPlayer());
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        checkEvent(event, event.getBlock().getLocation(), event.getPlayer());
    }

    @EventHandler
    public void onLiquidPlace(PlayerBucketEmptyEvent event) {
        checkEvent(event, event.getBlockClicked().getRelative(event.getBlockFace()).getLocation(), event.getPlayer());
    }

    @EventHandler
    public void onLiquidTake(PlayerBucketFillEvent event) {
        if (event.getBlockClicked().isLiquid()) {
            checkEvent(event, event.getBlockClicked().getLocation(), event.getPlayer());
        }
    }

    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        if (!Config.WILD_WORLDS.contains(player.getWorld().getName())) {
            return;
        }

        Bukkit.getScheduler().runTaskLater(GPWilderness.getPlugin(),
                () -> Lang.send(player, Lang.WILD_WORLD), 20);
    }

    private void checkEvent(Cancellable event, Location location, Player player) {
        if (!Config.PROTECT_WORLDS.contains(location.getWorld().getName())) {
            return; // world not protected
        }

        if (GPHook.isClaimed(location)) {
            return; // is claimed; ignore
        }

        if (player != null) {
            if (player.hasPermission("protection.bypass")) {
                return; // player bypasses protections
            }

            if (!notificationCooldown.contains(player.getUniqueId())) {
                Lang.send(player, Lang.WILDERNESS_PROTECTED);

                if (Config.NOTIFICATION_COOLDOWN > 0) {
                    notificationCooldown.add(player.getUniqueId());

                    new NotificationCooldown(player.getUniqueId())
                            .runTaskLater(GPWilderness.getPlugin(), Config.NOTIFICATION_COOLDOWN * 20);
                }
            }
        }

        if (Config.CANCEL_BELOW_SEA_LEVEL && location.getBlockY() <= location.getWorld().getSeaLevel()) {
            event.setCancelled(true);
        }

        if (Config.CANCEL_EVENTS) {
            event.setCancelled(true);
        }
    }

    private class NotificationCooldown extends BukkitRunnable {
        private UUID uuid;

        public NotificationCooldown(UUID uuid) {
            this.uuid = uuid;
        }

        @Override
        public void run() {
            notificationCooldown.remove(uuid);
        }
    }
}
